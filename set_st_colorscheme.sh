#!/bin/sh

# colorschemes are defined in $XDG_DATA_HOME/st/colorschemes

XDG_DATA_HOME="${XDG_DATA_HOME:-$HOME/.local/share}"
selected="$(ls "$XDG_DATA_HOME/st/colorschemes" | dmenu)"

[ -z "$selected" ] && exit 0
. "$XDG_DATA_HOME/st/colorschemes/$selected"

i=0
oldIFS=$IFS
IFS=" "
set -- $colors

# first 10 colors have a space before ':' for alignment
while [ "$i" -lt 10 ]; do
  color="${1%
}" # Remove a trailing newline.
  sed -i "s/^st.color${i}[[:space:]]*:.*/st.color${i} : $color/" \
    "$XDG_CONFIG_HOME/X11/Xresources"
  shift
  i=`expr $i + 1`
done

while [ "$i" -lt 16 ]; do
  color="${1%
}" # Remove a trailing newline.
  sed -i "s/^st.color${i}[[:space:]]*:.*/st.color${i}: $color/" \
    "$XDG_CONFIG_HOME/X11/Xresources"
  shift
  i=`expr $i + 1`
done

sed -i "s/^st.background[[:space:]]*:.*/st.background: $background/" \
  "$XDG_CONFIG_HOME/X11/Xresources"

sed -i "s/^st.foreground[[:space:]]*:.*/st.foreground: $foreground/" \
  "$XDG_CONFIG_HOME/X11/Xresources"

IFS=$oldIFS

xrdb "$XDG_CONFIG_HOME/X11/Xresources"
pidof st | xargs kill -s USR1

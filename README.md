st - simple terminal
====================
st is a simple terminal emulator for X which sucks less.

Motivation
==========

Here's an excerpt from the xterm(1) README:

    Abandon All Hope, Ye Who Enter Here

    This is undoubtedly the most ugly program in the distribution. It was one
    of the first "serious" programs ported, and still has a lot of historical
    baggage. Ideally, there would be a general tty widget and then vt102 and
    tek4014 subwidgets so that they could be used in other programs. We are
    trying to clean things up as we go, but there is still a lot of work to do.

It has over 65K lines of code and emulates obscure and obsolete terminals you
will never need.

The popular alternative, rxvt has only 32K lines of code. This is just too much
for something as simple as a terminal emulator; it's yet another example of
code complexity.

Terminal emulation doesn't need to be so complex.

About this fork
===============
This fork is based on plain st on commit 3a6d6d7401 (follows v0.9).

Applied patches (yes, i love applying patches):
- [Alpha](#alpha)
- [Anysize](#anysize)
- [Blinking cursor](#blinking-cursor)
- [Bold is not bright](#bold-is-not-bright)
- [Boxdraw](#boxdraw)
- [Change Alpha](#change-alpha)
- [Font2](#font2)
- [Keyboard select](#keyboard-select)
- [Ligatures (Boxdraw base)](#ligatures)
- [Netwmicon](#netwmicon)
- [Newterm](#newterm)
- [Scrollback mouse](#scrollback)
- [Scrollback mouse altscreen](#scrollback)
- [Scrollback mouse increment](#scrollback)
- [Scrollback ringbuffer](#scrollback)
- [Xresources signal reloading](#xresources-signal-reloading)

[Requirements](#requirements)
[Status](#status)
[Default bindings](#default-bindings)
[Configuration](#configuration)
[Installation](#installation)

[**Context aware copy**](#context-aware-copy)

Some quick notes about all patches:

Alpha
-----
This patch allows users to change the opacity of the background. Note that you
need an X composite manager (e.g. compton, xcompmgr) to make this patch
effective.

### Notes

- The alpha value affects the default background only.
- The color designated by 'defaultbg' should not be used elsewhere.
- Embedding might fail after applying this patch.

Anysize
-------
By default, st's window size always snaps to the nearest multiple of the
character size plus a fixed inner border (set with borderpx in config.h). When
the size of st does not perfectly match the space allocated to it (when using a
tiling WM, for example), unsightly gaps will appear between st and other apps,
or between instances of st.

This patch allows st to resize to any pixel size, makes the inner border size
dynamic, and centers the content of the terminal so that the left/right and
top/bottom borders are balanced. With this patch, st on a tiling WM will always
fill the entire space allocated to it.

Blinking cursor
---------------
This patch allows the use of a blinking cursor.

To demonstrate the available cursor styles, try these commands:

    echo -e -n "\x1b[\x30 q" # Blinking block
    echo -e -n "\x1b[\x31 q" # Blinking block (default)
    echo -e -n "\x1b[\x32 q" # Steady block
    echo -e -n "\x1b[\x33 q" # Blinking underline
    echo -e -n "\x1b[\x34 q" # Steady underline
    echo -e -n "\x1b[\x35 q" # Blinking bar
    echo -e -n "\x1b[\x36 q" # Steady bar
    echo -e -n "\x1b[\x37 q" # Blinking st cursor
    echo -e -n "\x1b[\x38 q" # Steady st cursor

When drawing is triggered, the cursor does not blink.

### Notes

- Only cursor styles 0, 1, 3, 5, and 7 blink. Set `cursorstyle` accordingly.
- Cursor styles are defined [here](https://invisible-island.net/xterm/ctlseqs/ctlseqs.html#h4-Functions-using-CSI-_-ordered-by-the-final-character-lparen-s-rparen:CSI-Ps-SP-q.1D81).

Bold is not bright
------------------
In `st`, bold text is rendered with a bold font in the bright variant of the
current color. This patch makes bold text rendered simply as bold, leaving
the color unaffected.

This patch has been modified to be toggled by `boldisbright` in `config.h`.

Boxdraw
-------
Graphic lines and blocks characters such as those used by `dialog`, `tree`,
`tmux` etc sometimes align with gaps - which doesn't look very nice. This can
depend on font, size, scaling, and other factors.

Braille is also increasingly used for graphics (`mapscii`, `vtop`, `gnuplot`,
etc), and may look or align nicer when rendered as "pixels" instead of dots.

This patch adds options to render most of the lines/blocks characters and/or
the braille ones without using the font so that they align perfectly regardless
of font, size or other configuration values.

Supported codepoints are U2500 - U259F except dashes and diagonals, and U28XX.

See also: unicode references [U2500.pdf](http://www.unicode.org/charts/PDF/U2500.pdf),
[U2580.pdf](http://www.unicode.org/charts/PDF/U2580.pdf),
[U2800.pdf](http://www.unicode.org/charts/PDF/U2800.pdf), and example pages
[UTF-8-demo.txt](https://www.cl.cam.ac.uk/~mgk25/ucs/examples/UTF-8-demo.txt) and
[libvte test page](https://github.com/GNOME/vte/blob/master/doc/boxes.txt).

Change Alpha
------------
A patch that allows for updating terminal transparency natively.

### Shortcuts:

    Alt+[          decrease alpha value
    Alt+]          increase alpha value
    Alt+Shift+[    reset alpha value

Font2
-----
This patch allows to add spare font besides default. Some glyphs can be not
present in default font. For these glyphs st uses font-config and tries to find
them in font cache first. This patch append fonts defined in `font2` variable
to the beginning of font cache. So they will be used first for glyphs that are
absent in default font.

Keyboard select
---------------
Enter this mode with `Ctrl+Shift+Escape`.

When you run "keyboard\_select", you have 3 modes available:

- move mode : to set the start of the selection;
- select mode : to activate and set the end of the selection;
- input mode : to enter the search criteria.

### Shortcuts for move and select modes:

    h, j, k, l:    move cursor left/down/up/right (also with arrow keys)
    !, _, *:       move cursor to the middle of the line/column/screen
    Backspace, $:  move cursor to the beginning/end of the line
    PgUp, PgDown : move cursor to the beginning/end of the column
    Home, End:     move cursor to the top/bottom left corner of the screen
    /, ?:          activate input mode and search up/down
    n, N:          repeat last search, up/down
    s:             toggle move/selection mode
    t:             toggle regular/rectangular selection type
    Return:        quit keyboard_select, keeping the highlight of the selection
    Escape:        quit keyboard_select

With `h, j, k, l` (also with arrow keys), you can use a quantifier. Enter a
number before hitting the appropriate key.

### Shortcuts for input mode:

    Return:        Return to the previous mode

Ligatures
---------
This patch adds proper drawing of ligatures.

The code uses Harfbuzz library to transform original text of a single line to a
list of glyphs with ligatures included.

### Notes

- A font with ligatures is needed.
- Due to some limitations in drawing engine, ligatures will break when crossing
  colors, font styles or selection. They will still render properly as separate
  symbols, just not as ligatures.
- Since 0.8.4 patch, there's now a way to enable additional font rendering
  features. Look into `features` array in `hb.c` for details.

Newterm
-------
Enables to set _NET_WM_ICON with a png-image.

Generally the icon of an application is defined by its desktop-entry. The patch
[desktopentry](https://st.suckless.org/patches/desktopentry) serves this
purpose. Unfortunately, some programs like
[tint2](https://gitlab.com/o9000/tint2) or
[alttab](https://github.com/sagb/alttab) can't make use of the desktop-entry
and rely instead on a hardcoded icon which has to be defined by the application
itself with the window-propery \_NET\_WM\_ICON. Since st doesn't define
\_NET\_WM\_ICON this programs can't display the correct icon for st even if a
desktop-entry exists. This patch solves this problem.

### Defining an icon
By default each time st starts it will search for a file with the name `st.png`
under `/usr/local/share/pixmaps/`. If you put an image with this name in the
root-directory of the st-repository and call `make install` the image will be
installed in `/usr/local/share/pixmaps/` automatically. Otherwise you have to
put the file there manually. You can try it out with this icon
[st.png](https://st.suckless.org/patches/netwmicon/st.png)
(credit: [flat-remix](https://github.com/daniruiz/flat-remix)).

Scrollback
----------
Scroll back through terminal output using `Shift+{Up, Down}`, `Alt+{k,j}` or
`MouseScroll`.

Scroll increment can be changed in `config.h`.

Xresources signal reloading
---------------------------
This patch adds the ability to configure st via Xresources and signal reloading.
This patch is **not** based on
[xresources patch](https://st.suckless.org/patches/xresources) and is extended
from [xst's commit on github](https://github.com/gnotclub/xst/commit/c0ffcfbaf8af25468103dd92e0c7e83555e08c7a).

You can basically pass a USR1 signal to all st processes after updating your
Xresources to reload the settings: `pidof st | xargs kill -s USR1`

Example Xresources:

    ! ligature font
    st.font: FiraCode Nerd Font:pixelsize=16,antialias=true,autohint=true
    st.alpha: 0.9
    st.color0 : #000000
    st.color1 : #e92f2f
    st.color2 : #0ed839
    st.color3 : #dddd13
    st.color4 : #3b48e3
    st.color5 : #f996e2
    st.color6 : #23edda
    st.color7 : #ababab
    st.color8 : #343434
    st.color9 : #e92f2f
    st.color10: #0ed839
    st.color11: #dddd13
    st.color12: #3b48e3
    st.color13: #f996e2
    st.color14: #23edda
    st.color15: #f9f9f9
    st.background: #000000
    st.foreground: #ababab

Requirements
============
In order to build st you need the Xlib header files.

### Additional Requirements from patches
- Harfbuzz (Ligatures)
- gd (Newterm)

Status
======

Things that have already been implemented are:

- most VT10X escape sequences
- serial line support
- XIM support
- utmp via utmp(1)
- clipboard handling
- mouse and keyboard shortcuts (via config.h)
- UTF-8
- wide-character support
- resize
- 256 colors and [true colors](https://gist.github.com/XVilka/8346728)
- antialiased fonts (using fontconfig)
- fallback fonts
- line drawing

See the [goals](https://st.suckless.org/goals) for more details and the TODO
for what still needs to be implemented or fixed.

### TODO
- Follow/copy links
- Copy output of commands
- Sixel support
- Integrate keyboard select in scrollback patch
- Add configuration from other patches to Xresources

Default bindings
================

<big><pre>
    Shift+PgUp         Zoom in
    Shift+PgDown       Zoom out
    Shift+Home         Zoom reset
    **Ctrl+c**         [Copy to clipboard](#context-aware-copy)
    Ctrl+Shift+c       Copy to clipboard
    **Ctrl+v**         [Paste clipboard](#context-aware-copy)
    Ctrl+Shift+y       Paste primary selection
    Shift+Insert       Paste primary selection
    Ctrl+Shift+Enter   Spawn new terminal on cwd
    Ctrl+Shift+Escape  Enter [keyboard select mode](#keyboard-select)
    Alt+k              Scroll up
    Alt+j              Scroll down
    Shift+Up           Scroll up
    Shift+Down         Scroll down
    Alt+[              Decrease alpha value
    Alt+]              Increase alpha value
    Alt+Shift+{        Reset alpha value
    MouseScrollUp      Scroll up
    MouseScrollDown    Scroll up
    Alt+s              Switch [colorschemes](#colorschemes)
</pre></big>

Context aware copy
==================
By default, copy is `Ctrl+c` and paste is `Ctrl+v`.

### What about SIGINT???
Don't worry, `Ctrl+c` still works as SIGINT. How? By only copying to clipboard
if there's an active selection. This means that if you select some text and use
`Ctrl+c` st will copy the selection to your clipboard, and if nothing is
selected `Ctrl+c` will send the usual SIGINT.

### What about inserting literal characters???
You can use `Ctrl+Shift+v` for that.

### Why???? How dare you!!!!
Because it's my fork ;D, if you *somehow* aren't mad by this and still want to
use my fork, but don't want this you can just remove the following codeblock in
`x.c`

    /* Context-aware copy */
    if (ksym == XK_c && match(ControlMask, e->state) &&
        getsel() != NULL) {
            xclipcopy();
            return;
    }

Also, change the following in `config.def.h`

    // From
    { ControlMask,          XK_v,           clippaste,      {.i =  0} },
    // To
    { TERMMOD,              XK_V,           clippaste,      {.i =  0} },

Configuration
=============

Configuration is done with config.h. Read the comments in the generated
config.h to edit it according to your needs. Defaults are stored in
config.def.h.

Installation
============
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if necessary as
root):

    make clean install

Colorschemes
============
To install colorschemes, execute:

    mkdir $XDG_DATA_HOME/st && cp -r colorschemes/ $XDG_DATA_HOME/st/

Make sure to add the following, if you haven't already, to your Xresources:

    st.color0 : #000000
    st.color1 : #e92f2f
    st.color2 : #0ed839
    st.color3 : #dddd13
    st.color4 : #3b48e3
    st.color5 : #f996e2
    st.color6 : #23edda
    st.color7 : #ababab
    st.color8 : #343434
    st.color9 : #e92f2f
    st.color10: #0ed839
    st.color11: #dddd13
    st.color12: #3b48e3
    st.color13: #f996e2
    st.color14: #23edda
    st.color15: #f9f9f9
    st.background: #000000
    st.foreground: #ababab

Running st
==========
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

Credits
=======
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

